require("dotenv").config();

const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const connection = require("../../db");

router.get("/program-type", (req, res) => {
  connection.query("SELECT * FROM `program_type`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

module.exports = router;
