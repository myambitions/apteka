require("dotenv").config();

const express = require("express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const connection = require("./db");
const axios = require("axios");

app.use(cors());
// const program = require("./routes/api/program");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Then pass them to cors:

app.get("/status", (req, res) => {
  res.send("Working!");
});

app.get("/pharm-net", (req, res) => {
  connection.query("SELECT * FROM `pharm_net`", (error, results) => {
    if (error) throw error;
    res.json(results);
  });
});

app.get("/manufacturer", (req, res) => {
  connection.query("SELECT * FROM `manufacturer`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

// get program api

app.get("/api/program", (req, res) => {
  connection.query("SELECT * FROM ncntv_program", (error, result) => {
    if (error) res.json({ error: "cant load program" });

    res.json(result);
  });
});

// get full program api

app.get("/api/full-program", (req, res) => {
  connection.query(
    "SELECT * FROM ncntv_program p INNER JOIN program_type t ON p.ncntv_program_program_type_id = t.program_type_id INNER JOIN manufacturer m ON p.ncntv_program_manufacturer_id = m.manufacturer_id;",
    (error, result) => {
      if (error) res.json({ error: "query error" });

      res.json(result);
    }
  );
});

app.get("/program-type", (req, res) => {
  connection.query("SELECT * FROM `program_type`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

app.get("/ncntv-scheme", (req, res) => {
  connection.query("SELECT * FROM `ncntv_scheme`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

app.get("/rebate", (req, res) => {
  connection.query("SELECT * FROM `rebate`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

app.get("/medicine", (req, res) => {
  connection.query("SELECT * FROM `medicine`", (error, result) => {
    res.json(result);
  });
});

app.get("/api/medicine/list", (req, res) => {
  connection.query(
    "SELECT medicine_id as value, medicine_name as label FROM `medicine`",
    (error, result) => {
      res.json(result);
    }
  );
});

app.get("/medicine-list", (req, res) => {
  connection.query("SELECT * FROM `medicine_list`", (error, result) => {
    if (error) throw error;

    res.json(result);
  });
});

// GET API FOR conditions
// Public

// condition/plan

app.get("/condition/plan", (req, res) => {
  connection.query(
    "SELECT * FROM apteka.condition_plan_item c INNER JOIN apteka.targetable t ON c.condition_plan_item_target_id = t.targetable_id INNER JOIN apteka.manufacturer m ON t.targetable_manufacturer_id = m.manufacturer_id INNER JOIN apteka.medicine e ON t.targetable_medicine_id = e.medicine_id",
    (error, result) => {
      if (error) {
        res.json(
          "cant reach condition plan. check connection or maybe some issues with db"
        );
      } else {
        res.json(result);
      }
    }
  );
});

// condition/stock

app.get("/condition/stock", (req, res) => {
  connection.query(
    "SELECT * FROM condition_stock c INNER JOIN condition_stock_item i ON c.condition_stock_id = i.condition_stock_item_stock_parent_id INNER JOIN apteka.targetable t ON i.condition_stock_item_target_id = t.targetable_id INNER JOIN apteka.manufacturer m ON t.targetable_manufacturer_id = m.manufacturer_id INNER JOIN apteka.medicine e ON t.targetable_medicine_id = e.medicine_id",
    (error, result) => {
      if (error) {
        res.json(
          "cant reach condition stock. check connection or maybe some issues with db"
        );
      } else {
        res.json(result);
      }
    }
  );
});

// condition/merch

app.get("/condition/merch", (req, res) => {
  connection.query("SELECT * FROM condition_merch", (error, result) => {
    if (error) {
      res.json(
        "cant reach condition merch. check connection or maybe some issues with db"
      );
    } else {
      res.json(result);
    }
  });
});

// condition/monthly

app.get("/condition/monthly", (req, res) => {
  connection.query("SELECT * FROM condition_monthly_event", (error, result) => {
    if (error) {
      res.json(
        "cant reach condition monthly. check connection or maybe some issues with db"
      );
    } else {
      res.json(result);
    }
  });
});

// GET API for /target
// Public

app.get("/api/target", (req, res) => {
  connection.query(
    "SELECT * FROM apteka.targetable t INNER JOIN apteka.manufacturer m ON t.targetable_manufacturer_id = m.manufacturer_id INNER JOIN apteka.medicine e ON t.targetable_medicine_id = e.medicine_id",
    (error, result) => {
      if (error) {
        res.json("cant get target from api");
      } else {
        res.json(result);
      }
    }
  );
});

// POST API FOR /rebate
// Public for now

app.post("/rebate/add", (req, res) => {
  let data = req.body;
  connection.query("INSERT INTO `rebate` SET ?", data, (error, result) => {
    if (error) {
      console.log("query error");
    }

    res.json(result);
  });
});

// POST API FOR /program
// Public for now

app.post("/program/add", (req, res) => {
  let data = req.body;
  connection.query(
    "INSERT INTO `ncntv_program` SET ?",
    data,
    (error, result) => {
      if (error) {
        console.log("query error");
      }

      res.json(result);
    }
  );
});

// POST API FOR /scheme
// Public for now

app.post("/scheme/add", (req, res) => {
  let data = req.body;
  connection.query(
    "INSERT INTO `ncntv_scheme` SET ?",
    data,
    (error, result) => {
      if (error) {
        console.log("query error");
      }

      res.json(result);
    }
  );
});

// POST API FOR /plan
// Public for now
app.post("/plan/add", (req, res) => {
  let data = req.body;
  connection.query(
    "INSERT INTO `condition_plan_item` SET ?",
    data,
    (error, result) => {
      if (error) {
        console.log("query error");
      }

      res.json(result);
    }
  );
});

// POST API FOR /condition-monthly-event
// Public for now
app.post("/condition-monthly-event/add", (req, res) => {
  let data = req.body;
  connection.query(
    "INSERT INTO `condition_monthly_event` SET ?",
    data,
    (error, result) => {
      if (error) {
        console.log("query error");
      }

      res.json(result);
    }
  );
});

// POST API FOR /condition-stock
// Public for now
app.post("/condition-stock/add", (req, res) => {
  let data = req.body;
  connection.query(
    "INSERT INTO `condition_stock` SET ?",
    data,
    (error, result) => {
      if (error) {
        res.json({ error: "post to /condition-stock failed" });
      }

      res.json(result);
    }
  );
});

app.get("/api/getToken", (req, res) => {
  const formUrlEncoded = x =>
    Object.keys(x).reduce(
      (p, c) => p + `&${c}=${encodeURIComponent(x[c])}`,
      ""
    );

  axios({
    method: "POST",
    url: "https://login.microsoftonline.com/common/oauth2/token",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    data: formUrlEncoded({
      grant_type: "password",
      scope: "openid",
      resource: "https://analysis.windows.net/powerbi/api",
      client_id: "bcea9dcb-8539-4218-8793-414ed920616e",
      username: "arman@azhkhanov.com",
      password: process.env.password,
      client_secret: process.env.client_secret
    })
  })
    .then(response => {
      return res.json(response.data);
    })
    .catch(function(error) {
      console.log(error);
    });
});

app.listen(3001, () => {
  console.log("Server running on 3001 port");
});
