const mysql = require("mysql");

let connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE
});

connection.connect(err => {
  if (err) {
    console.log("Error when connecting " + err.stack);
    return;
  }
  console.log("Connected as thread id " + connection.threadId);
});

module.exports = connection;
