# стэк

- React + Redux as `frontend`
- Node.js as `backend`
- MySQL queries

## как запускать

запускаем сервер
```shell
  cd backend && npm run server
```

запускаем фронт
```shell
  cd frontend && npm start
```


```shell
  если запускаете сервер и фронт на разных машинах, то желательно в файле frontend/package.json изменить поле proxy, на адрес серверной машины
```