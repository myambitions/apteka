import React, { Component } from "react";
import axios from "axios";

import * as pbi from "powerbi-client";

const powerbi = new pbi.service.Service(
  pbi.factories.hpmFactory,
  pbi.factories.wpmpFactory,
  pbi.factories.routerFactory
);

class PowerBi extends Component {
  constructor() {
    super();

    this.rootElement = null;

    this.state = {
      reportId: "656654c6-c5e5-43a5-b638-a127034c3a7e",
      embedUrl:
        "https://app.powerbi.com/reportEmbed?reportId=656654c6-c5e5-43a5-b638-a127034c3a7e&groupId=8c23139e-5e3c-4020-864a-6b6babdfc646",
      accessToken: false,
      type: "report"
    };
  }

  async componentDidMount() {
    await axios.get("/api/getToken").then(res => {
      this.setState({ accessToken: res.data.access_token });
    });

    let config = await this.state;

    await powerbi.embed(this.rootElement, config);
  }

  render() {
    return this.state.accessToken ? (
      <div
        className="powerbi-frame"
        ref={el => {
          this.rootElement = el;
        }}
        style={{
          width: "80%",
          height: "700px",
          margin: "25px auto",
          border: 0
        }}
      />
    ) : (
      "BI Loading ..."
    );
  }
}

export default PowerBi;
