import React, { Component } from "react";

import "./Register.css";
class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this.state);
  }

  render() {
    return (
      <div className="Register">
        <h1>Вход</h1>
        <p>Войдите под вашим аккаунтом</p>
        <form onSubmit={this.onSubmit}>
          <div className="form-control">
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.onChange}
            />
          </div>
          <div className="form-control">
            <input
              type="password"
              name="password"
              placeholder="Password"
              onChange={this.onChange}
            />
          </div>
          <button type="submit" className="btn btn-register">
            Submit
          </button>
        </form>
      </div>
    );
  }
}
export default Login;
