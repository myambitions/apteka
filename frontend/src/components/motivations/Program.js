import React, { Component } from "react";
import axios from "axios";

class Program extends Component {
  constructor(props) {
    super(props);

    this.state = {
      manufacturer: [],
      programType: [],

      ncntv_program_id: null,
      ncntv_program_name: "",
      ncntv_program_desc: "",
      ncntv_program_from_active_date: "",
      ncntv_program_to_active_date: "",
      ncntv_program_program_type_id: null,
      ncntv_program_manufacturer_id: null
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/manufacturer").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ manufacturer: loadedData });
    });
    axios.get("/program-type").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ programType: loadedData });
    });
  }

  onChange(e) {
    console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }

  // Save program handler
  saveProgram = e => {
    e.preventDefault();
    let data = {
      ncntv_program_id: null,
      ncntv_program_name: this.state.ncntv_program_name,
      ncntv_program_desc: this.state.ncntv_program_desc,
      ncntv_program_from_active_date: this.state.ncntv_program_from_active_date,
      ncntv_program_to_active_date: this.state.ncntv_program_to_active_date,
      ncntv_program_program_type_id: this.state.ncntv_program_program_type_id,
      ncntv_program_manufacturer_id: this.state.ncntv_program_manufacturer_id
    };

    axios
      .post("/program/add", data)
      .then(res => {
        console.log(res.data);
        this.setState({ step: 2 });
      })
      .catch(err => console.log(err));

    console.log(data);
  };

  render() {
    return (
      <div className="constructor">
        <div className="constructor-step">
          Создание программы
          <form className="constructor-form">
            <label htmlFor="ncntv_program_manufacturer_id">
              Выберите производителя:
            </label>
            <select
              name="ncntv_program_manufacturer_id"
              id="manufacturer"
              onChange={this.onChangeParseInt}
            >
              {this.state.manufacturer.map(manufacturer => {
                return (
                  <option
                    key={manufacturer.manufacturer_id}
                    value={manufacturer.manufacturer_id}
                  >
                    {manufacturer.manufacturer_name}
                  </option>
                );
              })}
            </select>

            <label htmlFor="ncntv_program_program_type_id">
              Выберите тип программы:
            </label>

            <select
              name="ncntv_program_program_type_id"
              id="ncntv_program_program_type_id"
              onChange={this.onChangeParseInt}
            >
              {this.state.programType.map(programType => {
                return (
                  <option
                    key={programType.program_type_id}
                    value={programType.program_type_id}
                  >
                    {programType.program_type_name}
                  </option>
                );
              })}
            </select>

            <label htmlFor="ncntv_program_name">
              Введите название программы
            </label>
            <input
              type="text"
              name="ncntv_program_name"
              placeholder="Название программы"
              onChange={this.onChange}
            />
            <label htmlFor="ncntv_program_desc">
              Введите описание программы
            </label>
            <textarea
              name="ncntv_program_desc"
              placeholder="Описание программы"
              onChange={this.onChange}
            />
            <label htmlFor="ncntv_program_from_active_date">От</label>
            <input
              type="date"
              name="ncntv_program_from_active_date"
              onChange={this.onChange}
            />
            <label htmlFor="ncntv_program_to_active_date">До</label>
            <input
              type="date"
              name="ncntv_program_to_active_date"
              onChange={this.onChange}
            />

            <button onClick={this.saveProgram}>Save Program</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Program;
