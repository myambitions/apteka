import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import AddProgram from "./ConstructorParts/AddProgram";

import "./Constructor.scss";

class Constructor extends Component {
  constructor() {
    super();

    this.state = {
      ncntvProgram: [],

      step: 1,
      program: [],
      scheme: [],
      plan: []
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/api/full-program").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ ncntvProgram: loadedData });
    });

    axios.get("/pharm-net").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ pharmNet: loadedData });
    });

    axios.get("/ncntv-scheme").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ ncntvScheme: loadedData });
    });
  }

  onChange(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }

  // Save condition plan handler

  render() {
    const allDone = (
      <div>
        <h3>Мотивация создана</h3>
        <Link to="/program">Мотивации</Link>
      </div>
    );

    let output;
    if (this.state.step === 1) {
      output = <AddProgram />;
    } else if (this.state.step === 2) {
      output = allDone;
    }

    return (
      <div className="container">
        <h1 className="header1">Конструктор мотиваций</h1>

        {output}
      </div>
    );
  }
}

export default Constructor;
