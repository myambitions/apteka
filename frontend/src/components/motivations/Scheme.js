import React, { Component } from "react";
import axios from "axios";

import Target from "./ConstructorParts/Target";

class Scheme extends Component {
  constructor() {
    super();

    this.state = {
      program: [],
      pharmNet: [],

      ncntv_scheme_id: null,
      ncntv_scheme_name: "",
      ncntv_scheme_desc: "",
      ncntv_scheme_ncntv_program_id: null,
      ncntv_scheme_target_pharm_net_id: null
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/pharm-net").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ pharmNet: loadedData });
    });

    axios.get("/api/program").then(res => {
      this.setState({ program: res.data });
    });
  }

  onChange(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }
  // Save scheme handler

  saveScheme = e => {
    e.preventDefault();
    let data = {
      ncntv_scheme_id: null,
      ncntv_scheme_name: this.state.ncntv_scheme_name,
      ncntv_scheme_desc: this.state.ncntv_scheme_desc,
      ncntv_program_id: this.state.ncntv_scheme_ncntv_program_id,
      ncntv_scheme_target_pharm_net_id: this.state
        .ncntv_scheme_target_pharm_net_id
    };
    axios
      .post("/scheme/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };
  render() {
    return (
      <div className="constructor-step">
        <hr />
        Создание схемы
        <form className="constructor-form">
          <p>Выберите программу</p>
          <select name="program" id="program">
            {this.state.program.map((program, id) => {
              return (
                <option key={id} value={program.ncntv_program_id}>
                  {program.ncntv_program_name}
                </option>
              );
            })}
          </select>

          <label htmlFor="ncntv_scheme_target_pharm_net_id">
            Выберите аптечную сеть:
          </label>
          <select
            name="ncntv_scheme_target_pharm_net_id"
            id="pharmNet"
            onChange={this.onChangeParseInt}
          >
            {this.state.pharmNet.map(pharmNet => {
              return (
                <option
                  key={pharmNet.pharm_net_id}
                  value={pharmNet.pharm_net_id}
                >
                  {pharmNet.pharm_net_name}
                </option>
              );
            })}
          </select>

          <label htmlFor="ncntv_scheme_name">Введите название схемы</label>
          <input
            type="text"
            name="ncntv_scheme_name"
            placeholder="Название схемы"
            onChange={this.onChange}
          />
          <label htmlFor="ncntv_scheme_desc">Введите описание схемы</label>
          <textarea
            name="ncntv_scheme_desc"
            placeholder="Описание схемы"
            onChange={this.onChange}
          />

          <hr />
          <button onClick={this.addConditionStock}>Save Scheme</button>
        </form>
        <Target />
      </div>
    );
  }
}

export default Scheme;
