import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import "./Motivations.scss";

class Motivations extends Component {
  state = {
    data: [],
    scheme: [],

    schemeList: false
  };

  componentDidMount() {
    axios.get("/api/full-program").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ data: loadedData });
    });
  }

  render() {
    const programs = this.state.data.map(data => {
      return (
        <Link
          to={`/motivations/program/${data.ncntv_program_id}`}
          key={data.id}
        >
          <ul className="table-body">
            <li className="col">{data.ncntv_program_id}</li>
            <li className="col">{data.ncntv_program_name}</li>
            <li className="col">{data.ncntv_program_desc}</li>
            <li className="col">{data.ncntv_program_from_active_date}</li>
            <li className="col">{data.ncntv_program_to_active_date}</li>
            <li className="col">{data.program_type_name}</li>
            <li className="col">{data.manufacturer_name}</li>
          </ul>
        </Link>
      );
    });

    return (
      <div className="motivations">
        <Link className="btn-default" to="/constructor">
          Создать мотивацию
        </Link>
        <div className="motivations-table">
          <div className="table">
            <div className="motivation-item">
              <ul className="table-body">
                <li className="col">#</li>
                <li className="col">Название программы</li>
                <li className="col">Описание программы</li>
                <li className="col">Дата начала</li>
                <li className="col">Дата конца</li>
                <li className="col">Тип программы</li>
                <li className="col">Имя производителя</li>
              </ul>
            </div>
            <div className="motivation-item">{programs}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Motivations;
