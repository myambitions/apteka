import React from "react";

import "./Conditions.scss";

class Conditions extends React.Component {
  render() {
    return (
      <div className="conditions">
        <div className="conditions-left">
          <p>Choose scheme</p>
          <select name="scheme" id="scheme">
            <option value="1">1</option>
          </select>

          <hr />

          <button>plan</button>
          <button>merch</button>
          <button>stock</button>
          <button>monthly</button>

          <div className="condition-output">
            <p>condition name</p>
            || ---- ||
          </div>
        </div>
        <div className="conditions-right">
          <div className="conditions-scheme">
            <p>|| here will be schemes ||</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Conditions;
