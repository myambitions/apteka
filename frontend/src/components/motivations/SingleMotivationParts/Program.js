import React from "react";
import { Link } from "react-router-dom";

import "./Program.scss";

class Program extends React.Component {
  state = {
    edit: false,
    saved: false
  };

  render() {
    const editProgram = e => {
      this.setState({
        edit: true,
        saved: false
      });
      e.preventDefault();
    };

    const saveProgram = e => {
      this.setState({
        edit: false,
        saved: true
      });
      e.preventDefault();
    };

    const closeProgram = e => {
      this.setState({
        edit: false,
        saved: false
      });
      e.preventDefault();
    };

    const deleteProgram = e => {
      alert("do you want to delete this program?");
      e.preventDefault();
    };

    const notEditable = (
      <div className="program-noteditable">
        <div className="program-labels">
          <p>Производитель:</p>
          <p>Тип программы:</p>
          <p>Описание программы:</p>
          <p>От:</p>
          <p>До:</p>
        </div>
        <div className="program-values">
          <p>{this.props.manufacturer}</p>
          <p>{this.props.type}</p>
          <p>{this.props.description}</p>
          <p>{this.props.from}</p>
          <p>{this.props.to}</p>
        </div>
      </div>
    );

    const editable = (
      <div className="program-noteditable">
        <div className="program-labels">
          <p>Производитель:</p>
          <p>Тип программы:</p>
          <p>Описание программы:</p>
          <p>От:</p>
          <p>До:</p>
        </div>
        <div className="program-values">
          <input value={this.props.manufacturer} />
          <input value={this.props.type} />
          <input value={this.props.description} />
          <input value={this.props.from} />
          <input value={this.props.to} />
        </div>
      </div>
    );

    return (
      <div className="program">
        <Link className="btn-default" to="/constructor/edit">
          Редактировать
        </Link>
        {this.state.edit ? editable : notEditable}
        {this.state.saved && <p className="red-notification">saved</p>}
      </div>
    );
  }
}

export default Program;
