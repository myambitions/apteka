import React from "react";

const ConditionMerch = props => {
  return (
    <div className="condition-merch">
      <p>Условие по мерчу №{props.merch.condition_merch_id}</p>
      <div className="merch-required">
        <span>Required:</span>
        {props.merch.condition_merch_is_required.data[0] === 1 ? (
          <input type="checkbox" disabled checked />
        ) : (
          <input type="checkbox" disabled />
        )}
      </div>
      <p>Option: {props.merch.condition_merch_option}</p>
      <a href={props.merch.condition_merch_desc_file}>download file</a>
      <p>or</p>
      <img src={props.merch.condition_merch_desc_file} alt="merch image" />
      <p>место для награды (rebate)</p>
    </div>
  );
};

export default ConditionMerch;
