import React from "react";

const ConditionPlan = props => {
  return (
    <div className="condition-plan">
      <p>Плановое условие #{props.plan.condition_plan_item_id}</p>
      <p>Measure type: {props.plan.condition_plan_item_measure_type}</p>
      <ul>
        Target #{props.plan.targetable_id}:
        <li>
          Производитель: <strong>{props.plan.manufacturer_name}</strong>
        </li>
        <li>
          Препарат: <strong>{props.plan.medicine_name}</strong>
        </li>
        <li>
          Список <strong>#{props.plan.targetable_medicine_list_id}</strong>
        </li>
      </ul>

      <div className="plan-required">
        <span>Required:</span>
        {props.plan.condition_plan_item_is_required.data[0] === 1 ? (
          <input type="checkbox" disabled checked />
        ) : (
          <input type="checkbox" disabled />
        )}
      </div>

      <div className="plan-focus">
        <span>Focus:</span>
        {props.plan.condition_plan_item_is_focus.data[0] === 1 ? (
          <input type="checkbox" disabled checked />
        ) : (
          <input type="checkbox" disabled />
        )}
      </div>

      <div className="plan-from-to">
        <p>от: {props.plan.condition_plan_item_from_bound}</p>
        <p>до: {props.plan.condition_plan_item_to_bound}</p>
      </div>
      <p>место для награды (rebate)</p>
    </div>
  );
};

export default ConditionPlan;

// condition_plan_item_id: 1
// condition_plan_item_measure_type: 1
// condition_plan_item_ncntv_scheme_id: 3
// condition_plan_item_rebate_id: 2
