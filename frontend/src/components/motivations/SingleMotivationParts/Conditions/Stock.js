import React from "react";

const ConditionStock = props => {
  return (
    <div className="condition-stock">
      <p>Стоковое условие №{props.stock.condition_stock_id}</p>

      <div className="stock-required">
        <span>Required:</span>
        {props.stock.condition_stock_is_required.data[0] === 1 ? (
          <input type="checkbox" disabled checked />
        ) : (
          <input type="checkbox" disabled />
        )}
      </div>

      <p>
        Warning_hours_count: {props.stock.condition_stock_warning_hours_count}
      </p>

      <ul>
        Target #{props.stock.targetable_id}:
        <li>
          Производитель: <strong>{props.stock.manufacturer_name}</strong>
        </li>
        <li>
          Препарат: <strong>{props.stock.medicine_name}</strong>
        </li>
        <li>
          Список <strong>#{props.stock.targetable_medicine_list_id}</strong>
        </li>
      </ul>
      <p>место для награды (rebate)</p>
    </div>
  );
};

export default ConditionStock;
