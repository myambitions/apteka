import React from "react";

import ConditionPlan from "./Plan";
import ConditionMerch from "./Merch";
import ConditionStock from "./Stock";
import ConditionMonthly from "./Monthly";

const Condition = props => {
  console.log(props);
  return (
    <div className="main-condition">
      {props.name === "plan" && <ConditionPlan plan={props.body} />}
      {props.name === "stock" && <ConditionStock stock={props.body} />}
      {props.name === "merch" && <ConditionMerch merch={props.body} />}
      {props.name === "monthly" && <ConditionMonthly monthly={props.body} />}

      <button>edit {props.name} condition</button>
      <button>delete</button>
    </div>
  );
};

export default Condition;
