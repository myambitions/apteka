import React from "react";

import "./Scheme.scss";

class Scheme extends React.Component {
  state = {
    edit: false,
    saved: false
  };

  render() {
    const editScheme = e => {
      this.setState({
        edit: true,
        saved: false
      });
      e.preventDefault();
    };

    const saveScheme = e => {
      this.setState({
        edit: false,
        saved: true
      });
      console.log("???");
      e.preventDefault();
    };

    const closeScheme = e => {
      this.setState({
        edit: false,
        saved: true
      });
      e.preventDefault();
    };

    const deleteScheme = e => {
      e.preventDefault();
    };

    const notEditable = (
      <div className="scheme">
        <div className="scheme-labels">
          <p>Аптечная сеть:</p>
          <p>Описание схемы:</p>
        </div>
        <div className="scheme-values">
          <p>{this.props.pharm}</p>
          <p>{this.props.description}</p>
        </div>
      </div>
    );

    const editable = (
      <div className="scheme">
        <div className="scheme-labels">
          <p>Аптечная сеть:</p>
          <p>Описание схемы:</p>
        </div>
        <div className="scheme-values">
          <input value={this.props.pharm} />
          <input value={this.props.description} />
        </div>
      </div>
    );

    return (
      <div>
        {this.state.edit ? editable : notEditable}
        {this.state.saved && <p className="red-notification">saved</p>}
      </div>
    );
  }
}

export default Scheme;
