import React, { Component } from "react";
import axios from "axios";

import Scheme from "./Scheme";
import Condition from "./Conditions/Condition";

import "./Schemes.scss";

class Schemes extends Component {
  constructor() {
    super();

    this.state = {
      scheme: [],
      pharmNet: [],
      loading: true,
      plan: [],
      stock: [],
      merch: [],
      monthly: [],
      currentCondition: [],
      currentConditionName: ""
    };
  }

  componentDidMount() {
    axios.get("/ncntv-scheme").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }

      this.setState({ scheme: loadedData });
      console.log("schemes loaded");
    });

    axios.get("/pharm-net").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }

      this.setState({ pharmNet: loadedData });
      console.log("pharm net loaded");
    });

    axios.get("/condition/plan").then(res => {
      const plan = [];
      for (let key in res.data) {
        plan.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({
        plan
      });
      console.log("plans added");
    });

    axios.get("/condition/stock").then(res => {
      const stock = [];
      for (let key in res.data) {
        stock.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({
        stock
      });
      console.log("stock added");
    });

    axios.get("/condition/merch").then(res => {
      const merch = [];
      for (let key in res.data) {
        merch.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({
        merch
      });
      console.log("merch added");
    });

    axios.get("/condition/monthly").then(res => {
      const monthly = [];
      for (let key in res.data) {
        monthly.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({
        monthly,
        loading: false
      });
      console.log("monthly added");
    });
  }

  updateSchemeOutput = () => {
    const singleScheme = this.state.scheme.filter(
      scheme => this.props.match.params.schemeId == scheme.ncntv_scheme_id
    );
    return singleScheme;
  };

  updatePlanOutput = () => {
    const plan = this.state.plan.filter(
      plan =>
        this.props.match.params.schemeId ==
        plan.condition_plan_item_ncntv_scheme_id
    );

    return plan;
  };

  updateStockOutput = () => {
    const stock = this.state.stock.filter(
      stock =>
        this.props.match.params.schemeId ==
        stock.condition_stock_ncntv_scheme_id
    );
    return stock;
  };

  updateMerchOutput = () => {
    const merch = this.state.merch.filter(
      merch =>
        this.props.match.params.schemeId ==
        merch.condition_merch_ncntv_scheme_id
    );

    return merch;
  };

  updateMonthlyOutput = () => {
    const monthly = this.state.monthly.filter(
      monthly =>
        this.props.match.params.schemeId ==
        monthly.condition_monthly_item_ncntv_scheme_id
    );

    return monthly;
  };

  updateCurrentState = (condition, name) => {
    this.setState({
      currentCondition: condition,
      currentConditionName: name
    });
  };

  render() {
    const { pharmNet, loading } = this.state;
    let schemeOutput = this.updateSchemeOutput();

    let newPlanOutput = this.updatePlanOutput();
    let newStockOutput = this.updateStockOutput();
    let newMerchOutput = this.updateMerchOutput();
    let newMonthlyOutput = this.updateMonthlyOutput();

    const planOutput = newPlanOutput.map((plan, id) => {
      return (
        <li key={id} onClick={() => this.updateCurrentState(plan, "plan")}>
          id: {plan.condition_plan_item_id}
        </li>
      );
    });

    const stockOutput = newStockOutput.map((stock, id) => {
      return (
        <li key={id} onClick={() => this.updateCurrentState(stock, "stock")}>
          id: {stock.condition_stock_id}
        </li>
      );
    });

    const merchOutput = newMerchOutput.map((merch, id) => {
      return (
        <li key={id} onClick={() => this.updateCurrentState(merch, "merch")}>
          id: {merch.condition_merch_id}
        </li>
      );
    });

    const monthlyOutput = newMonthlyOutput.map((monthly, id) => {
      return (
        <li
          key={id}
          onClick={() => this.updateCurrentState(monthly, "monthly")}
        >
          id: {monthly.condition_monthly_id}
        </li>
      );
    });

    const conditionsOutput = (
      <Condition
        body={this.state.currentCondition}
        name={this.state.currentConditionName}
      />
    );

    return (
      <div className="container">
        <div className="motivation-program" />
        {loading == false ? (
          <Scheme
            name={schemeOutput[0].ncntv_scheme_name}
            description={schemeOutput[0].ncntv_scheme_desc}
            pharm={pharmNet[0].pharm_net_name}
          />
        ) : (
          "Loading ..."
        )}
        <div className="motivation-schemes-list">
          <h2>Условия</h2>
          <div className="conditions">
            <div className="conditions-list">
              <p>Плановое условие</p>
              <ul>{planOutput}</ul>
              <p>Стоковое условие</p>
              <ul>{stockOutput}</ul>
              <p>Условие по мерчу</p>
              <ul>{merchOutput}</ul>
              <p>Месячное условие</p>
              <ul>{monthlyOutput}</ul>
            </div>
            <div className="conditions-output">
              {this.state.currentCondition.length === undefined
                ? conditionsOutput
                : "Choose condition"}
            </div>
          </div>
          <hr />
        </div>
      </div>
    );
  }
}

export default Schemes;
