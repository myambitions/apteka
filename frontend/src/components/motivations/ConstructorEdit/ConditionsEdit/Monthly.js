import React, { Component } from "react";

import Rebate from "../../ConstructorParts/Rebate";
import Target from "../../ConstructorParts/Target";

class ConditionMonthly extends Component {
  render() {
    return (
      <div className="constructor-step">
        <h2>Временное условие</h2> <button>+</button>
        <form className="constructor-form">
          <label htmlFor="condition_plan_item_rebate_id">
            Определите награду
          </label>
          <select
            name="condition_plan_item_rebate_id"
            id="rebate"
            onChange={this.onChangeParseInt}
          />
          required?
          <input type="checkbox" value="Required" />
          <Target />
          <label htmlFor="condition_monthly_event_discount">Discount</label>
          <input
            type="number"
            name="condition_monthly_event_discount"
            onChange={this.onChangeParseInt}
          />
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionMonthly}>Save</button>
        <button onClick={this.deleteConditionMonthly}>Clear</button>
      </div>
    );
  }
}

export default ConditionMonthly;
