import React, { Component } from "react";
import axios from "axios";

import Rebate from "../../ConstructorParts/Rebate";
import Target from "../../ConstructorParts/Target";

class ConditionStock extends Component {
  state = {
    stock: []
  };

  componentDidMount() {
    axios.get("/condition/stock").then(res => {
      this.setState({ stock: res.data });
    });
  }

  render() {
    return (
      <div className="constructor-step">
        <button onClick={() => console.log(this.state.stock)}>get state</button>
        <h2>Стоковое условие</h2> <button>+</button>
        <form className="constructor-form">
          <label htmlFor="condition_plan_item_rebate_id">
            Определите награду
          </label>
          <select
            name="condition_plan_item_rebate_id"
            id="rebate"
            onChange={this.onChangeParseInt}
          />
          required?
          <input type="checkbox" value="Required" />
          <Target />
          <label htmlFor="condition_stock_warning_hours_count">
            Warning_hours_count
          </label>
          <input
            type="number"
            name="condition_stock_warning_hours_count"
            onChange={this.onChangeParseInt}
          />
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionStock}>Save</button>
        <button onClick={this.deleteConditionStock}>Clear</button>
      </div>
    );
  }
}

export default ConditionStock;
