import React, { Component } from "react";
import axios from "axios";

import Rebate from "../../ConstructorParts/Rebate";
import Target from "../../ConstructorParts/Target";

class ConditionMerch extends Component {
  constructor() {
    super();
    this.state = {
      rebate: [],

      condition_plan_item_id: null,
      condition_plan_item_is_required: 1,
      condition_plan_item_target_id: 1,
      condition_plan_item_measure_type: "",
      condition_plan_item_from_bound: "",
      condition_plan_item_to_bound: "",
      condition_plan_item_is_focus: 1,
      condition_plan_item_rebate_id: null,
      condition_plan_item_ncntv_scheme_id: null
    };
  }

  componentDidMount() {
    axios.get("/rebate").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ rebate: loadedData });
    });
  }

  saveConditionMerch = e => {
    e.preventDefault();
    let data = {
      condition_plan_item_id: null,
      condition_plan_item_is_required: this.state
        .condition_plan_item_is_required,
      condition_plan_item_target_id: this.state.condition_plan_item_target_id,
      condition_plan_item_measure_type: this.state
        .condition_plan_item_measure_type,
      condition_plan_item_from_bound: this.state.condition_plan_item_from_bound,
      condition_plan_item_to_bound: this.state.condition_plan_item_to_bound,
      condition_plan_item_is_focus: this.state.condition_plan_item_is_focus,
      condition_plan_item_rebate_id: this.state.condition_plan_item_rebate_id,
      condition_plan_item_ncntv_scheme_id: this.state
        .condition_plan_item_ncntv_scheme_id
    };
    axios
      .post("/merch/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };

  render() {
    return (
      <div className="constructor-step">
        <h2>Условие по мерчу</h2> <button>+</button>
        <form className="constructor-form">
          {this.state.ncntv_scheme_name === "" ? (
            <h3 className="constructor-form-header">Введите название схемы!</h3>
          ) : (
            <h3 className="constructor-form-header">
              {this.state.ncntv_scheme_name}
            </h3>
          )}
          <label htmlFor="condition_plan_item_measure_type">Option</label>
          <input
            type="number"
            name="condition_plan_item_measure_type"
            placeholder="Option"
            onChange={this.onChangeParseInt}
          />
          <label htmlFor="isRequred">required?</label>
          <input type="checkbox" value="Required" />
          <div className="description-file">file</div>
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionMerch}>Save</button>
        <button onClick={this.deleteConditionMerch}>Clear</button>
      </div>
    );
  }
}

export default ConditionMerch;
