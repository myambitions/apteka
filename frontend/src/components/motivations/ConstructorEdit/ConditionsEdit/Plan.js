import React, { Component } from "react";
import axios from "axios";

import Rebate from "../../ConstructorParts/Rebate";
import Target from "../../ConstructorParts/Target";

class ConditionPlan extends Component {
  constructor() {
    super();
    this.state = {
      rebate: [],
      target: [],
      targetOutput: [],

      condition_plan_item_id: null,
      condition_plan_item_is_required: "",
      condition_plan_item_target_id: "",
      condition_plan_item_measure_type: "",
      condition_plan_item_from_bound: "",
      condition_plan_item_to_bound: "",
      condition_plan_item_is_focus: 1,
      condition_plan_item_rebate_id: null,
      condition_plan_item_ncntv_scheme_id: null
    };

    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
    this.updateTargetOutput = this.updateTargetOutput.bind(this);
  }

  async componentDidMount() {
    await axios.get("/rebate").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ rebate: loadedData });
    });

    await axios.get("/api/target").then(res => {
      this.setState({ target: res.data });
    });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }

  updateTargetOutput(e) {
    let targetOutput = this.state.target.filter(target => {
      return e.target.value == target.targetable_id;
    });

    this.setState({
      [e.target.name]: parseInt(e.target.value),
      targetOutput
    });
  }

  saveConditionPlan = e => {
    e.preventDefault();
    let data = {
      condition_plan_item_id: null,
      condition_plan_item_is_required: this.state
        .condition_plan_item_is_required,
      condition_plan_item_target_id: this.state.condition_plan_item_target_id,
      condition_plan_item_measure_type: this.state
        .condition_plan_item_measure_type,
      condition_plan_item_from_bound: this.state.condition_plan_item_from_bound,
      condition_plan_item_to_bound: this.state.condition_plan_item_to_bound,
      condition_plan_item_is_focus: this.state.condition_plan_item_is_focus,
      condition_plan_item_rebate_id: this.state.condition_plan_item_rebate_id,
      condition_plan_item_ncntv_scheme_id: this.state
        .condition_plan_item_ncntv_scheme_id
    };
    axios
      .post("/plan/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };

  render() {
    let targetOutput = this.state.targetOutput.length > 0 && (
      <ul>
        <li>
          Производитель:
          <strong>{this.state.targetOutput[0].manufacturer_name}</strong>
        </li>
        <li>
          Препарат: <strong>{this.state.targetOutput[0].medicine_name}</strong>
        </li>
        <li>
          Список:
          <strong>
            #{this.state.targetOutput[0].targetable_medicine_list_id}
          </strong>
        </li>
      </ul>
    );

    return (
      <div className="constructor-step">
        <h2>Плановое условие</h2> <button>+</button>
        <form className="constructor-form">
          {this.state.ncntv_scheme_name === "" ? (
            <h3 className="constructor-form-header">Введите название схемы!</h3>
          ) : (
            <h3 className="constructor-form-header">
              {this.state.ncntv_scheme_name}
            </h3>
          )}
          <label htmlFor="condition_plan_item_measure_type">Measure type</label>
          <input
            type="number"
            name="condition_plan_item_measure_type"
            placeholder="Measure type"
            onChange={this.onChangeParseInt}
          />
          <label htmlFor="condition_plan_item_target_id">Target</label>
          <div className="constructor-target">
            <Target />

            <div className="target-details">{}</div>
          </div>
          required?
          <input type="checkbox" value="Required" />
          focus?
          <input type="checkbox" value="Required" />
          <label htmlFor="condition_plan_item_from_bound">От</label>
          <input
            type="number"
            name="condition_plan_item_from_bound"
            onChange={this.onChangeParseInt}
          />
          <label htmlFor="condition_plan_item_to_bound">До</label>
          <input
            type="number"
            name="condition_plan_item_to_bound"
            onChange={this.onChangeParseInt}
          />
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionPlan}>Save</button>
        <button onClick={this.deleteConditionPlan}>Clear</button>
      </div>
    );
  }
}

export default ConditionPlan;
