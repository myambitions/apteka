import React, { Component } from "react";
import axios from "axios";

import ConditionPlan from "./ConditionsEdit/Plan";
import ConditionStock from "./ConditionsEdit/Stock";
import ConditionMonthly from "./ConditionsEdit/Monthly";
import ConditionMerch from "./ConditionsEdit/Merch";

class SchemeEdit extends Component {
  constructor() {
    super();

    this.state = {
      ncntvScheme: [],
      pharmNet: [],
      addConditionPlan: false,
      addConditionStock: false,
      addConditionMonthly: false,
      addConditionMerch: false,

      ncntv_scheme_id: null,
      ncntv_scheme_name: "",
      ncntv_scheme_desc: "",
      ncntv_scheme_ncntv_program_id: null,
      ncntv_scheme_target_pharm_net_id: null,

      condition: 0
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/pharm-net").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ pharmNet: loadedData });
    });
  }

  onChange(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }
  addConditionPlan = e => {
    e.preventDefault();
    this.setState({ condition: "plan" });
  };
  addConditionStock = e => {
    e.preventDefault();
    this.setState({ condition: "stock" });
  };
  addConditionMonthly = e => {
    e.preventDefault();
    this.setState({ condition: "monthly" });
  };
  addConditionMerch = e => {
    e.preventDefault();
    this.setState({ condition: "merch" });
  };
  deleteConditionPlan = e => {
    e.preventDefault();
    this.setState({ addConditionPlan: false });
  };
  deleteConditionStock = e => {
    e.preventDefault();
    this.setState({ addConditionStock: false });
  };
  deleteConditionMonthly = e => {
    e.preventDefault();
    this.setState({ addConditionMonthly: false });
  };

  // Save scheme handler

  saveScheme = e => {
    e.preventDefault();
    let data = {
      ncntv_scheme_id: null,
      ncntv_scheme_name: this.state.ncntv_scheme_name,
      ncntv_scheme_desc: this.state.ncntv_scheme_desc,
      ncntv_program_id: this.state.ncntv_scheme_ncntv_program_id,
      ncntv_scheme_target_pharm_net_id: this.state
        .ncntv_scheme_target_pharm_net_id
    };
    axios
      .post("/scheme/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };
  render() {
    return (
      <div className="constructor-step">
        <hr />
        <h2>Редактирование схемы №</h2>
        <form className="constructor-form">
          <label htmlFor="ncntv_scheme_target_pharm_net_id">
            Выберите аптечную сеть:
          </label>
          <select
            name="ncntv_scheme_target_pharm_net_id"
            id="pharmNet"
            onChange={this.onChangeParseInt}
          >
            {this.state.pharmNet.map(pharmNet => {
              return (
                <option
                  key={pharmNet.pharm_net_id}
                  value={pharmNet.pharm_net_id}
                >
                  {pharmNet.pharm_net_name}
                </option>
              );
            })}
          </select>

          <label htmlFor="ncntv_scheme_name">Введите название схемы</label>
          <input
            type="text"
            name="ncntv_scheme_name"
            placeholder="Название схемы"
            value={this.props.schemeName}
            onChange={this.onChange}
          />
          <label htmlFor="ncntv_scheme_desc">Введите описание схемы</label>
          <textarea
            name="ncntv_scheme_desc"
            placeholder="Описание схемы"
            value={this.props.schemeDescription}
            onChange={this.onChange}
          />
          <div className="conditions-tabs">
            <span
              className={this.state.condition == "plan" ? "active" : undefined}
              onClick={this.addConditionPlan}
            >
              Edit Condition Plan
            </span>
            <span
              className={this.state.condition == "stock" ? "active" : undefined}
              onClick={this.addConditionStock}
            >
              Edit Condition Stock
            </span>
            <span
              className={
                this.state.condition == "monthly" ? "active" : undefined
              }
              onClick={this.addConditionMonthly}
            >
              Edit Condition Monthly
            </span>
            <span
              className={this.state.condition == "merch" ? "active" : undefined}
              onClick={this.addConditionMerch}
            >
              Edit Condition Merch
            </span>
          </div>
          {/*<button onClick={this.addConditionStock}>Save Scheme</button>
          <button onClick={this.deleteScheme}>Delete Scheme</button>*/}
        </form>
        <div className="constructor-conditions">
          {this.state.condition == "plan" && <ConditionPlan />}
          {this.state.condition == "stock" && <ConditionStock />}
          {this.state.condition == "monthly" && <ConditionMonthly />}
          {this.state.condition == "merch" && <ConditionMerch />}
        </div>
      </div>
    );
  }
}

export default SchemeEdit;
