import React from "react";
import { Link } from "react-router-dom";

import "./ConstructorMenu.scss";

const ConstructorMenu = () => (
  <div className="constructor-links">
    <Link to="/constructor">Создать программу полного цикла</Link>
    <Link to="/program">Создать программу</Link>
    <Link to="/scheme">Создать схему к существующей программе</Link>
    <Link to="/condition">Создать условие</Link>
    <hr />
    <Link to="/rebate">Создать награды</Link>
    <Link to="/target">Создать цели</Link>
  </div>
);

export default ConstructorMenu;
