import React, { Component } from "react";
import axios from "axios";

import AddScheme from "./AddScheme";

class AddProgram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      manufacturer: [],
      programType: [],
      addScheme: false,

      ncntv_program_id: null,
      ncntv_program_name: "",
      ncntv_program_desc: "",
      ncntv_program_from_active_date: "",
      ncntv_program_to_active_date: "",
      ncntv_program_program_type_id: null,
      ncntv_program_manufacturer_id: null
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/manufacturer").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ manufacturer: loadedData });
    });
    axios.get("/program-type").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ programType: loadedData });
    });
  }

  onChange(e) {
    console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }

  // Add schemes and condition

  addScheme = e => {
    e.preventDefault();
    this.setState({ addScheme: true });
  };

  // Delete schemes and condition

  deleteScheme = e => {
    e.preventDefault();
    this.setState({ addScheme: false });
  };

  // Save program handler
  saveProgram = e => {
    e.preventDefault();
    let data = {
      ncntv_program_id: null,
      ncntv_program_name: this.state.ncntv_program_name,
      ncntv_program_desc: this.state.ncntv_program_desc,
      ncntv_program_from_active_date: this.state.ncntv_program_from_active_date,
      ncntv_program_to_active_date: this.state.ncntv_program_to_active_date,
      ncntv_program_program_type_id: this.state.ncntv_program_program_type_id,
      ncntv_program_manufacturer_id: this.state.ncntv_program_manufacturer_id
    };

    axios
      .post("/program/add", data)
      .then(res => {
        console.log(res.data);
        this.setState({ step: 2 });
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <div className="constructor">
        <h4>Создание программы</h4>
        <div className="constructor-step">
          <form className="constructor-form">
            <div className="form-group row">
              <label
                htmlFor="ncntv_program_manufacturer_id"
                class="col-sm-6 col-form-label"
              >
                Выберите производителя:
              </label>
              <div class="col-sm-6">
                <select
                  class="custom-select"
                  name="ncntv_program_manufacturer_id"
                  id="manufacturer"
                  onChange={this.onChangeParseInt}
                >
                  <option value="" selected disabled hidden>
                    Выберите производителя
                  </option>
                  {this.state.manufacturer.map(manufacturer => {
                    return (
                      <option
                        key={manufacturer.manufacturer_id}
                        value={manufacturer.manufacturer_id}
                      >
                        {manufacturer.manufacturer_name}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>

            <div className="form-group row">
              <label
                htmlFor="ncntv_program_program_type_id"
                class="col-sm-6 col-form-label"
              >
                Выберите тип программы:
              </label>
              <div class="col-sm-6">
                <select
                  class="custom-select"
                  name="ncntv_program_program_type_id"
                  id="ncntv_program_program_type_id"
                  onChange={this.onChangeParseInt}
                >
                  {this.state.programType.map(programType => {
                    return (
                      <option
                        key={programType.program_type_id}
                        value={programType.program_type_id}
                      >
                        {programType.program_type_name}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="ncntv_program_name"
                class="col-sm-6 col-form-label"
              >
                Введите название программы
              </label>
              <div class="col-sm-6">
                <input
                  class="form-control"
                  type="text"
                  name="ncntv_program_name"
                  placeholder="Название программы"
                  onChange={this.onChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="ncntv_program_desc"
                class="col-sm-6 col-form-label"
              >
                Введите описание программы
              </label>
              <div class="col-sm-6">
                <textarea
                  class="form-control"
                  name="ncntv_program_desc"
                  placeholder="Описание программы"
                  onChange={this.onChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="ncntv_program_from_active_date"
                class="col-sm-6 col-form-label"
              >
                От
              </label>
              <div class="col-sm-6">
                <input
                  class="form-control"
                  type="text"
                  name="ncntv_program_from_active_date"
                  onChange={this.onChange}
                />
                <small id="passwordHelpInline" class="text-muted">
                  Введите в формате дд/мм/гггг
                </small>
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="ncntv_program_to_active_date"
                class="col-sm-6 col-form-label"
              >
                До
              </label>
              <div class="col-sm-6">
                <input
                  class="form-control"
                  type="text"
                  name="ncntv_program_to_active_date"
                  onChange={this.onChange}
                />
                <small id="passwordHelpInline" class="text-muted">
                  Введите в формате дд/мм/гггг
                </small>
              </div>
            </div>

            <button onClick={this.addScheme}>Добавить схему</button>
            {/*
            <hr />
            <button onClick={this.saveProgram}>Save Program</button>*/}
          </form>
        </div>

        {this.state.addScheme ? (
          <AddScheme ncntv_program_name={this.state.ncntv_program_name} />
        ) : (
          false
        )}
      </div>
    );
  }
}

export default AddProgram;
