import React, { Component } from "react";
import axios from "axios";

import "./Rebate.scss";

class Rebate extends Component {
  constructor() {
    super();
    this.state = {
      state: "fixed",
      unfixed: "",

      rebate: "",
      manufacturer: [],
      medicine: [],
      medicine_list: [],

      option: "manufacturer"
    };
  }

  async componentDidMount() {
    await axios.get("/rebate").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ rebate: loadedData });
    });

    await axios.get("/manufacturer").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ manufacturer: loadedData });
    });

    await axios.get("/medicine").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ medicine: loadedData });
    });
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const fixed = (
      <div className="form-group row">
        <label htmlFor="rebate_amount" class="col-sm-6 col-form-label">
          Введите сумму:{" "}
        </label>
        <div class="col-sm-6">
          <input
            type="number"
            name="rebate_amount"
            placeholder="Введите cумму"
            class="form-control"
          />
        </div>
      </div>
    );

    const manufacturerList = (
      <select name="" class="form-control" onChange={this.onChange}>
        {this.state.manufacturer.map(item => {
          return (
            <option key={item.manufacturer_id} value={item.manufacturer_id}>
              {item.manufacturer_name}
            </option>
          );
        })}
      </select>
    );
    const medicineList = (
      <select name="" class="form-control" onChange={this.onChange}>
        {this.state.medicine.map(item => {
          return (
            <option key={item.medicine_id} value={item.medicine_id}>
              {item.medicine_name}
            </option>
          );
        })}
      </select>
    );
    const medicineListList = (
      <select name="" class="form-control" onChange={this.onChange}>
        {this.state.medicine_list.map(item => {
          return (
            <option key={item.medicine_list_id} value={item.medicine_list_id}>
              {item.medicine_list_medicine_id}
            </option>
          );
        })}
      </select>
    );

    const unfixedRebateLayout = (
      <Unfixed
        option={this.state.option}
        manufacturerList={manufacturerList}
        medicineList={medicineList}
        medicineListList={medicineListList}
        change={this.onChange}
        save={this.saveUnfixedRebate}
      />
    );

    return (
      <div>
        <form className="constructor-form">
          <p>Настроить награду за выполнение условия</p>
          <div className="form-group row">
            <label htmlFor="rebate_parent_id" class="col-sm-6 col-form-label">
              Тип:{" "}
            </label>
            <div class="col-sm-6">
              <select
                name="state"
                onChange={this.onChange}
                class="form-control"
              >
                <option value="fixed">Фиксированная</option>
                <option value="unfixed">Не фиксированная</option>
              </select>
            </div>
          </div>
          {this.state.state === "fixed" ? fixed : unfixedRebateLayout}
        </form>
      </div>
    );
  }
}

const Unfixed = props => (
  <div className="form-group row">
    <input
      type="number"
      name="rebate_percent"
      placeholder="Введите процент"
      class="form-control"
    />
    <select name="option" class="form-control" onChange={props.change}>
      <option value="manufacturer">Производитель</option>
      <option value="medicine">Препарат</option>
      <option value="medicine_list">Список препаратов</option>
    </select>
    {props.option === "manufacturer" && props.manufacturerList}
    {props.option === "medicine" && props.medicineList}
    {props.option === "medicine_list" && props.medicineListList}

    <button onClick={props.save}>Сохранить</button>
  </div>
);

export default Rebate;
