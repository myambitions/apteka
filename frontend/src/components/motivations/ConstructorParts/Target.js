import React from "react";
import axios from "axios";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";

import "./Target.scss";

class Target extends React.Component {
  state = {
    manufacturer: [],
    medicine: [],
    medicineList: [],

    manufacturer_id: "",
    medicine_id: "",
    medicine_list_id: "",

    target: 0
  };

  async componentDidMount() {
    await axios
      .all([
        axios.get("/manufacturer"),
        axios.get("/medicine"),
        axios.get("/api/medicine/list")
      ])
      .then(
        axios.spread((manufacturer, medicine, medicineList) => {
          this.setState({
            manufacturer: manufacturer.data,
            medicine: medicine.data,
            medicineList: medicineList.data
          });
        })
      );
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // target sdelat viborom iz 3x. User mozhet vibrat tolko 1 iz punktov
  render() {
    const manufacturer = (
      <div>
        <span>Выберите производителя:</span>
        <select name="manufacturer" id="manufacturer" class="form-control">
          {this.state.manufacturer.map((manufacturer, id) => {
            return <option key={id}>{manufacturer.manufacturer_name}</option>;
          })}
        </select>
      </div>
    );

    const medicine = (
      <div>
        <span>Выберите препарат:</span>
        <select name="medicine" id="medicine" class="form-control">
          {this.state.medicine.map((medicine, id) => {
            return <option key={id}>{medicine.medicine_name}</option>;
          })}
        </select>
      </div>
    );

    const medicineList = (
      <div className="medicine-list">
        <span>Выберите из списка:</span>
        <ReactMultiSelectCheckboxes
          class="form-control"
          options={this.state.medicineList}
        />
      </div>
    );

    return (
      <div className="col-sm-6">
        <div className="col-sm-12">
          <select
            name="target"
            onChange={this.onChange}
            className="form-control"
          >
            <option value="" selected disabled hidden>
              Выберите опцию
            </option>
            <option value="manufacturer">Производитель</option>
            <option value="medicine">Препарат</option>
            <option value="medicineList">Список препаратов</option>
          </select>
        </div>

        <div className="target-block">
          <div className="target-options">
            {this.state.target === "manufacturer" && manufacturer}
            {this.state.target === "medicine" && medicine}
            {this.state.target === "medicineList" && medicineList}
            <p className="red-notification">
              {this.state.target === 0 && "Please choose a target"}
            </p>
            {/* spisok dolzhen bit' -> checkboxi s viborom kuchi iz preparatov . radio button - pri vibore*/}
          </div>
        </div>
      </div>
    );
  }
}

export default Target;
