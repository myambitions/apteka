import React, { Component } from "react";
import axios from "axios";

import Rebate from "../Rebate";
import Target from "../Target";

class ConditionPlan extends Component {
  constructor() {
    super();
    this.state = {
      rebate: [],
      target: [],
      targetOutput: [],

      condition_plan_item_id: null,
      condition_plan_item_is_required: "",
      condition_plan_item_target_id: "",
      condition_plan_item_measure_type: "",
      condition_plan_item_from_bound: "",
      condition_plan_item_to_bound: "",
      condition_plan_item_is_focus: 1,
      condition_plan_item_rebate_id: null,
      condition_plan_item_ncntv_scheme_id: null
    };

    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
    this.updateTargetOutput = this.updateTargetOutput.bind(this);
  }

  async componentDidMount() {
    await axios.get("/rebate").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ rebate: loadedData });
    });

    await axios.get("/api/target").then(res => {
      this.setState({ target: res.data });
    });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }

  updateTargetOutput(e) {
    let targetOutput = this.state.target.filter(target => {
      return e.target.value == target.targetable_id;
    });

    this.setState({
      [e.target.name]: parseInt(e.target.value),
      targetOutput
    });
  }

  saveConditionPlan = e => {
    e.preventDefault();
    let data = {
      condition_plan_item_id: null,
      condition_plan_item_is_required: this.state
        .condition_plan_item_is_required,
      condition_plan_item_target_id: this.state.condition_plan_item_target_id,
      condition_plan_item_measure_type: this.state
        .condition_plan_item_measure_type,
      condition_plan_item_from_bound: this.state.condition_plan_item_from_bound,
      condition_plan_item_to_bound: this.state.condition_plan_item_to_bound,
      condition_plan_item_is_focus: this.state.condition_plan_item_is_focus,
      condition_plan_item_rebate_id: this.state.condition_plan_item_rebate_id,
      condition_plan_item_ncntv_scheme_id: this.state
        .condition_plan_item_ncntv_scheme_id
    };
    axios
      .post("/plan/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };

  render() {
    let targetOutput = this.state.targetOutput.length > 0 && (
      <ul>
        <li>
          Производитель:
          <strong>{this.state.targetOutput[0].manufacturer_name}</strong>
        </li>
        <li>
          Препарат: <strong>{this.state.targetOutput[0].medicine_name}</strong>
        </li>
        <li>
          Список:
          <strong>
            #{this.state.targetOutput[0].targetable_medicine_list_id}
          </strong>
        </li>
      </ul>
    );

    return (
      <div className="constructor-step">
        <form className="constructor-form">
          <div className="form-group row">
            <label
              htmlFor="condition_plan_item_measure_type"
              class="col-sm-6 col-form-label"
            >
              Measure type
            </label>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="condition_plan_item_measure_type"
                id="inlineRadio1"
                value="option1"
              />
              <label class="form-check-label" for="inlineRadio1">
                тенге
              </label>
            </div>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="condition_plan_item_measure_type"
                id="inlineRadio2"
                value="option2"
              />
              <label class="form-check-label" for="inlineRadio2">
                кг
              </label>
            </div>
          </div>

          <div className="form-group row">
            <label
              htmlFor="condition_plan_item_target_id"
              class="col-sm-6 col-form-label"
            >
              Цель
            </label>
            <Target />
          </div>
          <div className="form-group row col-sm-6">
            <div class="col-sm-3">
              <input
                class="form-control"
                type="number"
                name="condition_plan_item_from_bound"
                placeholder="От"
                onChange={this.onChangeParseInt}
              />
            </div>
            <div class="col-sm-3">
              <input
                class="form-control"
                type="number"
                name="condition_plan_item_to_bound"
                placeholder="До"
                onChange={this.onChangeParseInt}
              />
            </div>
          </div>

          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="required-condition"
            />
            <label class="form-check-label" for="required-condition">
              Обязательное условие
            </label>
          </div>
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="focus-condition"
            />
            <label class="form-check-label" for="focus-condition">
              Focus
            </label>
          </div>
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionPlan}>Сохранить</button>
        <button onClick={this.deleteConditionPlan}>Очистить</button>
      </div>
    );
  }
}

export default ConditionPlan;
