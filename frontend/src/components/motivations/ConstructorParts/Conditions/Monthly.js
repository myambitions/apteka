import React, { Component } from "react";

import Rebate from "../Rebate";
import Target from "../Target";

class ConditionMonthly extends Component {
  render() {
    return (
      <div className="constructor-step">
        <form className="constructor-form">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="required-condition"
            />
            <label class="form-check-label" for="required-condition">
              Обязательное условие
            </label>
          </div>
          <div className="form-group row">
            <label
              htmlFor="condition_stock_warning_hours_count"
              class="col-sm-6 col-form-label"
            >
              Discount
            </label>
            <div class="col-sm-6">
              <input
                class="form-control"
                type="number"
                name="condition_stock_warning_hours_count"
                onChange={this.onChangeParseInt}
              />
            </div>
          </div>

          <div className="form-group row">
            <label
              htmlFor="condition_plan_item_target_id"
              class="col-sm-6 col-form-label"
            >
              Цель
            </label>
            <Target />
          </div>
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionMonthly}>Сохранить</button>
        <button onClick={this.deleteConditionMonthly}>Очистить</button>
      </div>
    );
  }
}

export default ConditionMonthly;
