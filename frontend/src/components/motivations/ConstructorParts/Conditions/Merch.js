import React, { Component } from "react";
import axios from "axios";

import Rebate from "../Rebate";
import "./Merch.css";

class ConditionMerch extends Component {
  constructor() {
    super();
    this.state = {
      rebate: [],

      condition_plan_item_id: null,
      condition_plan_item_is_required: 1,
      condition_plan_item_target_id: 1,
      condition_plan_item_measure_type: "",
      condition_plan_item_from_bound: "",
      condition_plan_item_to_bound: "",
      condition_plan_item_is_focus: 1,
      condition_plan_item_rebate_id: null,
      condition_plan_item_ncntv_scheme_id: null
    };
  }

  componentDidMount() {
    axios.get("/rebate").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ rebate: loadedData });
    });
  }

  saveConditionMerch = e => {
    e.preventDefault();
    let data = {
      condition_plan_item_id: null,
      condition_plan_item_is_required: this.state
        .condition_plan_item_is_required,
      condition_plan_item_target_id: this.state.condition_plan_item_target_id,
      condition_plan_item_measure_type: this.state
        .condition_plan_item_measure_type,
      condition_plan_item_from_bound: this.state.condition_plan_item_from_bound,
      condition_plan_item_to_bound: this.state.condition_plan_item_to_bound,
      condition_plan_item_is_focus: this.state.condition_plan_item_is_focus,
      condition_plan_item_rebate_id: this.state.condition_plan_item_rebate_id,
      condition_plan_item_ncntv_scheme_id: this.state
        .condition_plan_item_ncntv_scheme_id
    };
    axios
      .post("/merch/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };

  render() {
    return (
      <div className="constructor-step">
        <form className="constructor-form">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="required-condition"
            />
            <label class="form-check-label" for="required-condition">
              Обязательное условие
            </label>
          </div>
          <div className="form-group row">
            {this.state.ncntv_scheme_name === "" ? (
              <h3 className="constructor-form-header">
                Введите название схемы
              </h3>
            ) : (
              <h3 className="constructor-form-header">
                {this.state.ncntv_scheme_name}
              </h3>
            )}
            <label
              htmlFor="condition_plan_item_measure_type"
              class="col-sm-6 col-form-label"
            >
              Option
            </label>
            <div class="col-sm-6">
              <input
                class="form-control"
                type="number"
                name="condition_plan_item_measure_type"
                placeholder="Option"
                onChange={this.onChangeParseInt}
              />
            </div>
          </div>

          <div className="description-file">файл</div>
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionMerch}>Сохранить</button>
        <button onClick={this.deleteConditionMerch}>Очистить</button>
      </div>
    );
  }
}

export default ConditionMerch;
