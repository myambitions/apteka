import React, { Component } from "react";
import axios from "axios";

import Rebate from "../Rebate";
import Target from "../Target";

class ConditionStock extends Component {
  state = {
    stock: []
  };

  componentDidMount() {
    axios.get("/condition/stock").then(res => {
      this.setState({ stock: res.data });
    });
  }

  render() {
    return (
      <div className="constructor-step">
        <form className="constructor-form">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="required-condition"
            />
            <label class="form-check-label" for="required-condition">
              Обязательное условие
            </label>
          </div>

          <div className="form-group row">
            <label
              htmlFor="condition_plan_item_target_id"
              class="col-sm-6 col-form-label"
            >
              Цель
            </label>
            <Target />
          </div>

          <div className="form-group row">
            <label
              htmlFor="condition_stock_warning_hours_count"
              class="col-sm-6 col-form-label"
            >
              Warning_hours_count
            </label>
            <div class="col-sm-6">
              <input
                class="form-control"
                type="number"
                name="condition_stock_warning_hours_count"
                onChange={this.onChangeParseInt}
              />
            </div>
          </div>

          <div className="form-group row">
            <label
              htmlFor="condition_plan_item_measure_type"
              class="col-sm-6 col-form-label"
            >
              count
            </label>
            <div class="col-sm-6">
              <input
                class="form-control"
                type="number"
                name="condition_stock_warning_hours_count"
                onChange={this.onChangeParseInt}
              />
            </div>
          </div>
        </form>
        <hr />
        <Rebate />
        <hr />
        <button onClick={this.addConditionStock}>Сохранить</button>
        <button onClick={this.deleteConditionStock}>Очистить</button>
      </div>
    );
  }
}

export default ConditionStock;
