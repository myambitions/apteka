import React, { Component } from "react";
import axios from "axios";

import ConditionPlan from "./Conditions/Plan";
import ConditionStock from "./Conditions/Stock";
import ConditionMonthly from "./Conditions/Monthly";
import ConditionMerch from "./Conditions/Merch";

import "./Scheme.scss";

class AddScheme extends Component {
  constructor() {
    super();

    this.state = {
      ncntvScheme: [],
      pharmNet: [],
      addConditionPlan: false,
      addConditionStock: false,
      addConditionMonthly: false,
      addConditionMerch: false,

      ncntv_scheme_id: null,
      ncntv_scheme_name: "",
      ncntv_scheme_desc: "",
      ncntv_scheme_ncntv_program_id: null,
      ncntv_scheme_target_pharm_net_id: null,

      condition: 0
    };
    this.onChange = this.onChange.bind(this);
    this.onChangeParseInt = this.onChangeParseInt.bind(this);
  }

  componentDidMount() {
    axios.get("/pharm-net").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      this.setState({ pharmNet: loadedData });
    });
  }

  onChange(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onChangeParseInt(e) {
    // console.log(e.target.name + ": " + e.target.value);
    this.setState({
      [e.target.name]: parseInt(e.target.value)
    });
  }
  addConditionPlan = e => {
    e.preventDefault();
    this.setState({ condition: "plan" });
  };
  addConditionStock = e => {
    e.preventDefault();
    this.setState({ condition: "stock" });
  };
  addConditionMonthly = e => {
    e.preventDefault();
    this.setState({ condition: "monthly" });
  };
  addConditionMerch = e => {
    e.preventDefault();
    this.setState({ condition: "merch" });
  };
  deleteConditionPlan = e => {
    e.preventDefault();
    this.setState({ addConditionPlan: false });
  };
  deleteConditionStock = e => {
    e.preventDefault();
    this.setState({ addConditionStock: false });
  };
  deleteConditionMonthly = e => {
    e.preventDefault();
    this.setState({ addConditionMonthly: false });
  };

  // Save scheme handler

  saveScheme = e => {
    e.preventDefault();
    let data = {
      ncntv_scheme_id: null,
      ncntv_scheme_name: this.state.ncntv_scheme_name,
      ncntv_scheme_desc: this.state.ncntv_scheme_desc,
      ncntv_program_id: this.state.ncntv_scheme_ncntv_program_id,
      ncntv_scheme_target_pharm_net_id: this.state
        .ncntv_scheme_target_pharm_net_id
    };
    axios
      .post("/scheme/add", data)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => console.log(err));

    console.log(data);
  };
  render() {
    return (
      <div className="constructor">
        <h4>Создание схемы</h4>
        <div className="constructor-step">
          <form className="constructor-form">
            <div className="form-group row">
              <label
                htmlFor="ncntv_scheme_target_pharm_net_id"
                class="col-sm-6 col-form-label"
              >
                Выберите аптечную сеть:
              </label>
              <div class="col-sm-6">
                <select
                  class="custom-select"
                  name="ncntv_scheme_target_pharm_net_id"
                  id="pharmNet"
                  onChange={this.onChangeParseInt}
                >
                  <option selected disabled hidden>
                    Выбрать...
                  </option>
                  {this.state.pharmNet.map(pharmNet => {
                    return (
                      <option
                        key={pharmNet.pharm_net_id}
                        value={pharmNet.pharm_net_id}
                      >
                        {pharmNet.pharm_net_name}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>

            <div className="form-group row">
              <label
                htmlFor="ncntv_scheme_name"
                class="col-sm-6 col-form-label"
              >
                Введите название схемы
              </label>
              <div class="col-sm-6">
                <input
                  class="form-control"
                  type="text"
                  name="ncntv_scheme_name"
                  placeholder="Название схемы"
                  onChange={this.onChange}
                />
              </div>
            </div>
            
            <div className="form-group row">
              <label
                htmlFor="ncntv_scheme_desc"
                class="col-sm-6 col-form-label"
              >
                Введите описание схемы
              </label>
              <div class="col-sm-6">
                <textarea
                  class="form-control"
                  name="ncntv_scheme_desc"
                  placeholder="Описание схемы"
                  onChange={this.onChange}
                />
              </div>
            </div>
          </form>
        </div>
        <div className=" conditions-tabs">
          <span
            className={this.state.condition == "plan" ? "active" : undefined}
            onClick={this.addConditionPlan}
          >
            Плановое условие
          </span>
          <span
            className={this.state.condition == "stock" ? "active" : undefined}
            onClick={this.addConditionStock}
          >
            Стоковое условие
          </span>
          <span
            className={this.state.condition == "monthly" ? "active" : undefined}
            onClick={this.addConditionMonthly}
          >
            Месячное условие
          </span>
          <span
            className={this.state.condition == "merch" ? "active" : undefined}
            onClick={this.addConditionMerch}
          >
            Условие по мерчу
          </span>
        </div>
        <div className="constructor-step constructor-conditions">
          {this.state.condition == "plan" && <ConditionPlan />}
          {this.state.condition == "stock" && <ConditionStock />}
          {this.state.condition == "monthly" && <ConditionMonthly />}
          {this.state.condition == "merch" && <ConditionMerch />}
        </div>
      </div>
    );
  }
}

export default AddScheme;
