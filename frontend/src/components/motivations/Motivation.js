import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import axios from "axios";

import Program from "./SingleMotivationParts/Program";
import Schemes from "./SingleMotivationParts/Schemes";

import "./Constructor.scss";

class Motivation extends Component {
  constructor() {
    super();

    this.state = {
      ncntvProgram: [],
      ncntvScheme: [],
      programLoaded: false,
      schemesLoaded: false
    };
  }

  async componentDidMount() {
    await axios.get("/api/full-program").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }
      const ncntvProgram = loadedData.filter(program => {
        return program.ncntv_program_id == this.props.match.params.programId;
      });

      this.setState({ ncntvProgram });
    });

    await axios.get("/ncntv-scheme").then(res => {
      const loadedData = [];
      for (let key in res.data) {
        loadedData.push({
          ...res.data[key],
          id: key
        });
      }

      const ncntvScheme = loadedData.filter(scheme => {
        return (
          scheme.ncntv_program_id ===
          this.state.ncntvProgram[0].ncntv_program_id
        );
      });

      this.setState({ ncntvScheme, schemesLoaded: true });
    });
  }

  render() {
    const {
      ncntvProgram,
      ncntvScheme,
      programLoaded,
      schemesLoaded
    } = this.state;
    const schemeExists =
      ncntvScheme.length > 0 ? (
        this.state.ncntvScheme.map((scheme, id) => {
          return (
            <li key={id}>
              <Link
                to={`/motivations/program/${
                  this.props.match.params.programId
                }/${scheme.ncntv_scheme_id}`}
              >
                {scheme.ncntv_scheme_name}
              </Link>
            </li>
          );
        })
      ) : (
        <div>There is no schemes :)</div>
      );
    return (
      <div className="container">
        <div className="motivation-program">
          <h1>
            Программа:{" "}
            {ncntvProgram.length > 0 && ncntvProgram[0].ncntv_program_name}
          </h1>
          {ncntvProgram.length > 0 ? (
            <Program
              manufacturer={ncntvProgram[0].manufacturer_name}
              type={ncntvProgram[0].program_type_name}
              description={ncntvProgram[0].ncntv_program_desc}
              to={ncntvProgram[0].ncntv_program_to_active_date}
              from={ncntvProgram[0].ncntv_program_from_active_date}
            />
          ) : (
            "Loading ..."
          )}
        </div>
        <div className="motivation-schemes-list">
          <h2>Схемы</h2>
          <hr />
          <ul>{schemesLoaded == false ? "Loading ..." : schemeExists}</ul>
        </div>

        <Route
          exact
          path={`/motivations/program/${
            this.props.match.params.programId
          }/:schemeId`}
          component={Schemes}
        />
      </div>
    );
  }
}

export default Motivation;
