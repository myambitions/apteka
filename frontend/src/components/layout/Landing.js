import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Landing.css";

class Landing extends Component {
  render() {
    return (
      <div className="Landing">
        <div className="Landing-wrapper">
          <div className="Heading">
            <h1>Apteka</h1>
          </div>
          <div className="Landing-buttons">
            <Link to="/login" className="Landing-Login">
              Вход
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
