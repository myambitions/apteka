import React from "react";
import { Link } from "react-router-dom";

export default () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link to="/" className="navbar-brand">
          <img
            src="/docs/4.0/assets/brand/bootstrap-solid.svg"
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt=""
          />{" "}
          Apteka
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarText"
          aria-controls="navbarText"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link to="/" className="nav-link">
                Главная
                <span className="sr-only">(сейчас)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/motivations" className="nav-link">
                Мотивации
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/embedded-bi" className="nav-link">
                Аналитика
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Вход
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};
