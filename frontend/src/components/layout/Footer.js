import React from "react";

import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-credentials">
        All rights reserved &copy; {new Date().getFullYear()}. Apteka
      </div>
    </div>
  );
};

export default Footer;
