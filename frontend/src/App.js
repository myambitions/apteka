import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Header from "./components/layout/Header";
import Footer from "./components/layout/Footer";
import Landing from "./components/layout/Landing";
import Login from "./components/register/Login";
import Motivations from "./components/motivations/Motivations";
import Motivation from "./components/motivations/Motivation";
import Condition from "./components/motivations/SingleMotivationParts/Conditions/Condition";
import Constructor from "./components/motivations/Constructor";
import ConstructorMenu from "./components/motivations/ConstructorMenu";
import Scheme from "./components/motivations/Scheme";
import Program from "./components/motivations/Program";
import Conditions from "./components/motivations/Conditions/Conditions";
import Rebate from "./components/motivations/ConstructorParts/Rebate";
import Target from "./components/motivations/ConstructorParts/Target";
import ConstructorEdit from "./components/motivations/ConstructorEdit/ConstructorEdit";
import PowerBi from "./components/additions/PowerBi";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/motivations" component={Motivations} />
            <Route exact path="/constructor-menu" component={ConstructorMenu} />
            <Route exact path="/constructor" component={Constructor} />
            <Route exact path="/constructor/edit" component={ConstructorEdit} />
            <Route exact path="/program" component={Program} />
            <Route exact path="/condition" component={Conditions} />
            <Route exact path="/rebate" component={Rebate} />
            <Route exact path="/target" component={Target} />
            <Route exact path="/embedded-bi" component={PowerBi} />

            <Route
              path="/motivations/program/:programId"
              component={Motivation}
            />

            <Route exact path="/scheme" component={Scheme} />
            <Route
              exact
              path={`/motivations/program/:programId/:schemeId/:planId`}
              component={Condition}
            />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
